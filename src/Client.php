<?php
namespace MindOfMicah\Bitbucket;
use GuzzleHttp\Client as Guzzle;

class Client
{
    public function __construct(array $options = [])
    {
        $options = array_merge(['base_uri'=>'https://api.bitbucket.org/2.0/'], $options);  
        $this->client = new Guzzle($options);
    }

    public function createRepository($username, $repo_name, array $options = [])
    {
        return $this->client->request('POST', 'repositories/' . $username . '/' . $repo_name, [
            'json' => $options
        ]);
    }

    public function enablePipeline($username, $repo_name)
    {
        return $this->client->request('PUT', 'repositories/'.$username.'/' . $repo_name . '/pipelines_config', [
        'json'=>[
            'enabled'=>true
        ]
    ]);

    }

    public function addEnvironmentVariable($username, $repo_name, $key, $value)
    {
        return $this->client->request('POST', 'repositories/'.$username.'/' . $repo_name . '/pipelines_config/variables/', [
            'json'=>[
                'value'=>$value,
                'secured'=>false,
                'key'=>$key,
            ]
        ]);

    }
    public function addSSHKeys($username, $repo_name, $private, $public)
    {
        return $this->client->request('PUT', 'repositories/'.$username.'/' . $repo_name . '/pipelines_config/ssh/key_pair', [
            'json'=>[
                'private_key'=>$private,
                'public_key'=>$public,
            ]
        ]);

    }

    public function addKnownHost($username, $repo_name, $host_name, $public_key)
    {
        return $this->client->request('POST', 'repositories/mindofmicah/'.$repo_name.'/pipelines_config/ssh/known_hosts/', [
        'json'=>[
            'hostname'=>$host_name,
//            'type'=>'pipeline_known_host',
 //           'uuid'=>uniqid(),
            'public_key'=>[
                'key_type'=> 'ssh-rsa',
                'key'=>base64_Encode($public_key)
            ]
            
      ]
    ]);

    }
}
